Group: group 76

Question
========

RQ: Is there any difference in the average period of alliance with the bank(in months) between male and female?

Null hypothesis: There is no difference in the mean of the period of alliance with the bank between male and female.

Alternative hypothesis: There is a difference in the mean of the period of alliance with the bank between male and female.

Dependent Variable: Months_on_book.

Independent Variable: Gender.

Dataset
=======

URL: https://www.kaggle.com/sakshigoyal7/credit-card-customers

Column Headings:

```

> colnames(BankChurners)
 [1] "CLIENTNUM"                                                                                                                         
 [2] "Attrition_Flag"                                                                                                                    
 [3] "Customer_Age"                                                                                                                      
 [4] "Gender"                                                                                                                            
 [5] "Dependent_count"                                                                                                                   
 [6] "Education_Level"                                                                                                                   
 [7] "Marital_Status"                                                                                                                    
 [8] "Income_Category"                                                                                                                   
 [9] "Card_Category"                                                                                                                     
[10] "Months_on_book"                                                                                                                    
[11] "Total_Relationship_Count"                                                                                                          
[12] "Months_Inactive_12_mon"                                                                                                            
[13] "Contacts_Count_12_mon"                                                                                                             
[14] "Credit_Limit"                                                                                                                      
[15] "Total_Revolving_Bal"                                                                                                               
[16] "Avg_Open_To_Buy"                                                                                                                   
[17] "Total_Amt_Chng_Q4_Q1"                                                                                                              
[18] "Total_Trans_Amt"                                                                                                                   
[19] "Total_Trans_Ct"                                                                                                                    
[20] "Total_Ct_Chng_Q4_Q1"                                                                                                               
[21] "Avg_Utilization_Ratio"                                                                                                             
[22] "Naive_Bayes_Classifier_Attrition_Flag_Card_Category_Contacts_Count_12_mon_Dependent_count_Education_Level_Months_Inactive_12_mon_1"
[23] "Naive_Bayes_Classifier_Attrition_Flag_Card_Category_Contacts_Count_12_mon_Dependent_count_Education_Level_Months_Inactive_12_mon_2"

```